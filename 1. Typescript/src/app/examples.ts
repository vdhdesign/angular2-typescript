//////////////////////////////////
// Beauty of 'Header files'
// Example intellisense
//////////////////////////////////
/// <reference path="../types/libs/jquery.d.ts"/>
/// <reference path="../types/libs/greensock.d.ts"/>

// TweenLite.to();
//$(".my-div").position().left
















//////////////////////////////////
// Interfaces
//////////////////////////////////


interface Config1 {
	width: number;
	color: string;
}

var myConfig1: Config1 = { width: 10, color: "#fff" };
// var myConfig1_1: Config1 = { width: 10}; //Not going to work




interface Config2 {
	width: number;
	color?: string;
}

var myConfig2: Config2 = { width: 10 }; //Will work now that color is optional































//////////////////////////////////
// Class Example
// (Very similar to ActionScript3)
//////////////////////////////////


////Using normal constructor, setting paramaters to member variables
// class MyNameClass
// {
// 	public name: string = "";
//     constructor(_name: string)
//     {
// 		this.name = _name;
//     }
// }
// var mattsName = new MyNameClass("matt");
// log(mattsName.name);
// mattsName.name = "bob";
// log(mattsName.name);




////Using typescript automatic Member variable creation and assignment 
// class MyNameClass
// {
// 	//Note public
//     constructor(public name: string) {
//     }
// }
// var mattsName = new MyNameClass("matt");
// log(mattsName.name);
// mattsName.name = "bob";
// log(mattsName.name);



//// Using a getter (Nice!) 
// class MyNameClass
// {
// 	//   Automatically sets as private member variable
//     constructor(private m_name: string)
//     {
//     }
// 	//   Only readable via get
//     get name()
//     {
// 		return this.m_name;
//     }
// }
// var mattsName = new MyNameClass("matt");
// log(mattsName.name);
// mattsName.name = "bob";
// log(mattsName.name);


























//////////////////////////////////
// Inheritance
// (Very similar to ActionScript3)
//////////////////////////////////

// class Animal {
//     protected name: string;

//     constructor(theName: string)
//     {
//     	this.name = theName;
//     }

//     move(meters: number = 0)
//     {
//         console.log(this.name + " moved " + meters + "m.");
//     }
// }

// class Snake extends Animal
// {
// 	public laugh()
// 	{
// 		console.log("laugh");
// 	}
//     constructor(name: string)
//     {
//     	super(name);
//     }

//     move(meters = 5)
//     {
//         console.log("Slithering...");
//         super.move(meters);
//     }
// }

// class Horse extends Animal
// {
//     constructor(name: string)
//     { 
//     	super(name);
//     }

//     move(meters = 45)
//     {
//         console.log("Galloping...");
//         super.move(meters);
//     }
// }

// var sam: Snake = new Snake("Sammy the Python");
// var tom: Animal = new Horse("Tommy the Palomino");

// sam.move();
// tom.move(34);
























//////////////////////////////////
// Enums
// (Yay for enums!)
//////////////////////////////////


enum SomeRandomEnum { Test1, Test2 };
enum Colour { Red, Green, Blue };

class SolidColourRainbow
{
	constructor(_solidColour: Colour)
	{
		//
	}
}

// var scr = new SolidColourRainbow("#ff0");
// var scr = new SolidColourRainbow(0); //Works as enums are just integers (but dont do that)
// var scr = new SolidColourRainbow(SomeRandomEnum.Test1); //Wont work, even though both values are integers, 
												           //it's checking the enum type
// var scr = new SolidColourRainbow(Colour.Red);
















//////////////////////////////////
// Static Properties
//////////////////////////////////


// class MyClass {
// 	private static NUM_CLASS_INSTANCES: number = 0; 

// 	constructor()
// 	{
// 		++MyClass.NUM_CLASS_INSTANCES;
// 	}

// 	static get numClassInstances()
// 	{
// 		return MyClass.NUM_CLASS_INSTANCES;
// 	}
// }

// console.log("number of instances: " + MyClass.numClassInstances);
// var myClass = new MyClass();
// console.log("number of instances: " + MyClass.numClassInstances);
// var myClass2 = new MyClass();
// var myClass3 = new MyClass();
// var myClass4 = new MyClass();
// console.log("number of instances: " + MyClass.numClassInstances);

